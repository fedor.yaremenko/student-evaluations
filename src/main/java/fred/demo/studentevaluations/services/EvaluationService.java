package fred.demo.studentevaluations.services;

import fred.demo.studentevaluations.dtos.AverageEvaluationDto;
import fred.demo.studentevaluations.dtos.UpdateEvaluationDto;
import fred.demo.studentevaluations.mapping.MappingHelper;
import fred.demo.studentevaluations.model.Student;
import fred.demo.studentevaluations.model.Subject;
import fred.demo.studentevaluations.repositories.SubjectRepository;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class EvaluationService {

    private final MappingHelper mappingHelper;
    private final SubjectRepository subjectRepository;

    public EvaluationService(MappingHelper mappingHelper, SubjectRepository subjectRepository) {
        this.mappingHelper = mappingHelper;
        this.subjectRepository = subjectRepository;
    }

    /**
     * Обновить оценку по предмету
     * @param subject предмет
     * @param updateEvaluationDto DTO для маппинга
     */
    public void updateEvaluation(Subject subject, UpdateEvaluationDto updateEvaluationDto) {
        mappingHelper.mapDtoToBean(updateEvaluationDto, subject);
    }

    /**
     * Получить среднюю оценку студента по предметам
     * @param student студент
     * @return средняя оценка; null если нет предметов, по которым проставлены оценки
     */
    public AverageEvaluationDto getAverageEvaluation(Student student) {
        BigDecimal avgEvaluation = subjectRepository.getAvgEvaluationByStudent(student);
        AverageEvaluationDto averageEvaluationDto = new AverageEvaluationDto();
        averageEvaluationDto.setAverageEvaluation(avgEvaluation);
        return averageEvaluationDto;
    }

}

package fred.demo.studentevaluations.services;

import fred.demo.studentevaluations.dtos.SubjectDto;
import fred.demo.studentevaluations.exceptions.AlreadyExistsException;
import fred.demo.studentevaluations.exceptions.NotFoundException;
import fred.demo.studentevaluations.mapping.MappingHelper;
import fred.demo.studentevaluations.model.Student;
import fred.demo.studentevaluations.model.Subject;
import fred.demo.studentevaluations.repositories.SubjectRepository;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Сервис управления предметами студентов
 */
@Service
public class SubjectService {

    private final MappingHelper mappingHelper;
    private final SubjectRepository subjectRepository;

    public SubjectService(MappingHelper mappingHelper, SubjectRepository subjectRepository) {
        this.mappingHelper = mappingHelper;
        this.subjectRepository = subjectRepository;
    }

    /**
     * Получение списка предметов студента
     * @param student студент
     * @return предметы
     */
    public List<Subject> findAll(Student student) {
        return subjectRepository.findByStudent(student);
    }

    /**
     * Получить объект предмета по студенту и идентификатору
     * @param student студент, к которому привязан предмет
     * @param id идентификатор предмета
     * @return объект предмета
     * @throws NotFoundException предмет с заданным идентификатором не найден
     */
    public Subject get(Student student, long id) throws NotFoundException {
        return subjectRepository.findByStudentAndId(student, id).orElseThrow(NotFoundException::new);
    }

    private void assertTitleConflict(Student student, long idNot, SubjectDto subjectDto) throws AlreadyExistsException {
        if (subjectRepository.existsByIdNotAndStudentAndTitle(idNot, student, subjectDto.getTitle())) {
            throw new AlreadyExistsException();
        }
    }

    private void assertTitleConflict(Student student, SubjectDto subjectDto) throws AlreadyExistsException {
        if (subjectRepository.existsByStudentAndTitle(student, subjectDto.getTitle())) {
            throw new AlreadyExistsException();
        }
    }

    /**
     * Привязать новый предмет к студенту
     * @param student студент
     * @param subjectDto DTO для маппинга
     * @return хранимый объект студента
     * @throws AlreadyExistsException к студенту уже привязан предмет с заданным названием
     */
    public Subject create(Student student, SubjectDto subjectDto) throws AlreadyExistsException {
        assertTitleConflict(student, subjectDto);

        Subject subject = mappingHelper.mapDtoToBean(subjectDto);
        student.addSubject(subject);
        subjectRepository.save(subject);
        return subject;
    }

    /**
     * Обновить информацию о предмете
     * @param student студент
     * @param id идентификатор предмета
     * @param subjectDto DTO для маппинга
     * @return объект предмета
     * @throws NotFoundException не найден предмет, прявязанный к студенту, с заданным идентификатором
     * @throws AlreadyExistsException к студенту уже привязан предмет с заданным названием
     */
    public Subject update(Student student, long id, SubjectDto subjectDto) throws NotFoundException, AlreadyExistsException {
        Subject subject = get(student, id);
        assertTitleConflict(student, id, subjectDto);

        mappingHelper.mapDtoToBean(subjectDto, subject);
        return subject;
    }

    /**
     * Удалить предмет у студента
     * @param student студент
     * @param id идентификатор предмета
     * @throws NotFoundException не найден предмет, прявязанный к студенту, с заданным идентификатором
     */
    public void delete(Student student, long id) throws NotFoundException {
        Subject subject = get(student, id);
        subjectRepository.delete(subject);
    }
}

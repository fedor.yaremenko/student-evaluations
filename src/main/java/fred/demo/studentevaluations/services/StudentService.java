package fred.demo.studentevaluations.services;

import fred.demo.studentevaluations.dtos.StudentDto;
import fred.demo.studentevaluations.exceptions.AlreadyExistsException;
import fred.demo.studentevaluations.exceptions.NotFoundException;
import fred.demo.studentevaluations.mapping.MappingHelper;
import fred.demo.studentevaluations.model.Student;
import fred.demo.studentevaluations.repositories.StudentRepository;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Сервис управления студентами
 */
@Service
public class StudentService {

    private final MappingHelper mappingHelper;
    private final StudentRepository studentRepository;

    public StudentService(MappingHelper mappingHelper, StudentRepository studentRepository) {
        this.mappingHelper = mappingHelper;
        this.studentRepository = studentRepository;
    }

    /**
     * Получить объект студента по идентификатору
     * @param id идентификатор студента
     * @return объект студента
     * @throws NotFoundException студент с заданным идентификатором не найден
     */
    public Student get(long id) throws NotFoundException {
        return studentRepository.findById(id).orElseThrow(NotFoundException::new);
    }

    private void assertNameConflict(long idNot, StudentDto studentDto) throws AlreadyExistsException {
        if (studentRepository.existsByIdNotAndLastNameAndFirstNameAndSurName(
                idNot, studentDto.getLastName(), studentDto.getFirstName(), studentDto.getSurName())) {
            throw new AlreadyExistsException();
        }
    }

    private void assertNameConflict(StudentDto studentDto) throws AlreadyExistsException {
        if (studentRepository.existsByLastNameAndFirstNameAndSurName(
                studentDto.getLastName(), studentDto.getFirstName(), studentDto.getSurName())) {
            throw new AlreadyExistsException();
        }
    }

    /**
     * Создание студента
     * @param studentDto DTO для маппинга
     * @return хранимый объект студента
     * @throws AlreadyExistsException студент с заданными ФИО уже существует
     */
    public Student create(StudentDto studentDto) throws AlreadyExistsException {
        assertNameConflict(studentDto);

        Student student = mappingHelper.mapDtoToBean(studentDto);
        studentRepository.save(student);
        return student;
    }

    /**
     * Обновление информации о студенте
     * @param id идентификатор студента
     * @param studentDto DTO для маппинга
     * @return хранимый объект студента
     * @throws NotFoundException студент с заданным идентификатором не найден
     * @throws AlreadyExistsException студент с заданными ФИО уже существует
     */
    public Student update(long id, StudentDto studentDto) throws NotFoundException, AlreadyExistsException {
        assertNameConflict(id, studentDto);

        Student student = get(id);
        mappingHelper.mapDtoToBean(studentDto, student);
        return student;
    }

    /**
     * Удаление информации о студенте
     * @param id идентификатор студента
     * @throws NotFoundException студент с заданным идентификатором не найден
     */
    public void delete(long id) throws NotFoundException {
        Student student = get(id);
        studentRepository.delete(student);
    }

    /**
     * Получение списка всех студентов
     * @return все студенты
     */
    public List<Student> findAll() {
        return studentRepository.findAll();
    }
}

package fred.demo.studentevaluations;

public class Constants {

    public static final String STUDENTS_PATH = "/students";
    public static final String SUBJECTS_PATH = STUDENTS_PATH + "/{studentId}/subjects";
    public static final String AVG_EVALUATION_SUBPATH = "/avgEvaluation";
    public static final String UPDATE_EVALUATION_SUBPATH = "/updateEvaluation";

    public static final int MAX_STRING_LENGTH = 255;

}

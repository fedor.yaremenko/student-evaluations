package fred.demo.studentevaluations.repositories;

import fred.demo.studentevaluations.model.Student;
import fred.demo.studentevaluations.model.Subject;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Repository
public interface SubjectRepository extends CrudRepository<Subject, Long> {

    List<Subject> findByStudent(Student student);

    Optional<Subject> findByStudentAndId(Student student, long id);

    boolean existsByStudentAndTitle(Student student, String title);

    boolean existsByIdNotAndStudentAndTitle(long idNot, Student student, String title);

    @Query("select avg(evaluation) from Subject s where student = ?1")
    BigDecimal getAvgEvaluationByStudent(Student student);
}

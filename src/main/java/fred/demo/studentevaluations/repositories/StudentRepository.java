package fred.demo.studentevaluations.repositories;

import fred.demo.studentevaluations.model.Student;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepository extends CrudRepository<Student, Long> {

    @Override
    List<Student> findAll();

    boolean existsByLastNameAndFirstNameAndSurName(String lastName, String firstName, String surName);

    boolean existsByIdNotAndLastNameAndFirstNameAndSurName(long idNot, String lastName, String firstName, String surName);

}

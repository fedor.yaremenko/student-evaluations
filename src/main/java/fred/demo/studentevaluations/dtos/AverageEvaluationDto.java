package fred.demo.studentevaluations.dtos;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.math.BigDecimal;

@Schema(name = "AverageEvaluation", description = "Средняя оценка студента")
public class AverageEvaluationDto {

    @Schema(description = "Средняя оценка", example = "9.23", accessMode = Schema.AccessMode.READ_ONLY)
    @Min(1)
    @Max(10)
    private BigDecimal AverageEvaluation;

    public BigDecimal getAverageEvaluation() {
        return AverageEvaluation;
    }

    public void setAverageEvaluation(BigDecimal averageEvaluation) {
        this.AverageEvaluation = averageEvaluation;
    }
}

package fred.demo.studentevaluations.dtos;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import static fred.demo.studentevaluations.Constants.MAX_STRING_LENGTH;

@Schema(name = "Student", description = "Студент")
public class StudentDto {

    @Schema(accessMode = Schema.AccessMode.READ_ONLY)
    private Long id;

    @Schema(description = "Фамилия", example = "Иванов")
    @NotBlank
    @Size(max = MAX_STRING_LENGTH)
    private String lastName;

    @Schema(description = "Имя", example = "Иванов")
    @NotBlank
    @Size(max = MAX_STRING_LENGTH)
    private String firstName;

    @Schema(description = "Отчество", example = "Иванович")
    @NotBlank
    @Size(max = MAX_STRING_LENGTH)
    private String surName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

}

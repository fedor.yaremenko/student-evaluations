package fred.demo.studentevaluations.dtos;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Schema(name = "UpdateEvaluation", description = "Оценка студента по предмету")
public class UpdateEvaluationDto {

    @Schema(description = "Оценка", example = "9")
    @Min(1)
    @Max(10)
    private Integer evaluation;

    public Integer getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(Integer evaluation) {
        this.evaluation = evaluation;
    }
}

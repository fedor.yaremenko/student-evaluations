package fred.demo.studentevaluations.dtos;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.*;

import static fred.demo.studentevaluations.Constants.MAX_STRING_LENGTH;

@Schema(name = "Subject", description = "Предмет")
public class SubjectDto {

    @Schema(accessMode = Schema.AccessMode.READ_ONLY)
    private Long id;

    @Schema(description = "Название предмета", example = "Функциональный анализ")
    @NotBlank
    @Size(max = MAX_STRING_LENGTH)
    private String title;

    @Schema(description = "Оценка по предмету", example = "9", accessMode = Schema.AccessMode.READ_ONLY)
    @Min(1)
    @Max(10)
    private Integer evaluation;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(Integer evaluation) {
        this.evaluation = evaluation;
    }
}

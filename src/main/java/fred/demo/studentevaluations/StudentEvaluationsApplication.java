package fred.demo.studentevaluations;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudentEvaluationsApplication {

    public static void main(String[] args) {
        SpringApplication.run(StudentEvaluationsApplication.class, args);
    }

}

package fred.demo.studentevaluations.controllers;

import fred.demo.studentevaluations.exceptions.*;
import io.swagger.v3.oas.annotations.Hidden;
import org.h2.jdbc.JdbcSQLIntegrityConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@Hidden
public class GlobalControllerExceptionHandler {

    @ExceptionHandler(AlreadyExistsException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public void handleConflict() { }

    /*
    Спорное решение. Т.к. в H2 нет аналога sp_getapplock, то не можем сделать блокировку на уровне приложения,
    из-за этого может возникнуть ситуация, когда записываем одновременно два одинаковых студента или предмета
     */
    @ExceptionHandler(JdbcSQLIntegrityConstraintViolationException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public void handleDbConflict() { }

    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public void handleNotFound() { }

}

package fred.demo.studentevaluations.controllers;

import fred.demo.studentevaluations.dtos.SubjectDto;
import fred.demo.studentevaluations.exceptions.AlreadyExistsException;
import fred.demo.studentevaluations.exceptions.NotFoundException;
import fred.demo.studentevaluations.mapping.MappingHelper;
import fred.demo.studentevaluations.model.Student;
import fred.demo.studentevaluations.model.Subject;
import fred.demo.studentevaluations.services.StudentService;
import fred.demo.studentevaluations.services.SubjectService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

import static fred.demo.studentevaluations.Constants.SUBJECTS_PATH;

@RestController
@RequestMapping(SUBJECTS_PATH)
@Tag(name = "Предметы", description = "Управление предметами студентов")
public class SubjectController {

    private final static String HTTP_200_DESCR = "OK";
    private final static String HTTP_201_DESCR = "Предмет добавлен";
    private final static String HTTP_400_DESCR = "Неправильно сформированный запрос";
    private final static String HTTP_404_STUDENT_DESCR = "Студент не найден";
    private final static String HTTP_404_STUDENT_OR_SUBJECT_DESCR = "Студент или предмет не найден";
    private final static String HTTP_409_DESCR = "Предмет с заданными названием ранее уже добавлен студенту";

    private final static String STUDENT_ID_DESCR = "Идентификатор студента";
    private final static String SUBJECT_ID_DESCR = "Идентификатор предмета";

    private final MappingHelper mappingHelper;
    private final StudentService studentService;
    private final SubjectService subjectService;

    public SubjectController(MappingHelper mappingHelper, StudentService studentService, SubjectService subjectService) {
        this.mappingHelper = mappingHelper;
        this.studentService = studentService;
        this.subjectService = subjectService;
    }

    @GetMapping
    @Operation(summary = "Получить список всех предметов студента")
    @ApiResponse(responseCode = "200", description = HTTP_200_DESCR)
    @ApiResponse(responseCode = "404", description = HTTP_404_STUDENT_DESCR, content = @Content)
    @Transactional(readOnly = true)
    public List<SubjectDto> list(@PathVariable("studentId") @Parameter(description = STUDENT_ID_DESCR) long studentId)
            throws NotFoundException {
        Student student = studentService.get(studentId);
        return subjectService.findAll(student).stream()
                .map(mappingHelper::mapBeanToDto)
                .collect(Collectors.toList());
    }

    @GetMapping("/{subjectId}")
    @Operation(summary = "Получить информацию о предмете")
    @ApiResponse(responseCode = "200", description = HTTP_200_DESCR)
    @ApiResponse(responseCode = "404", description = HTTP_404_STUDENT_OR_SUBJECT_DESCR, content = @Content)
    @Transactional(readOnly = true)
    public SubjectDto read(@PathVariable("studentId") @Parameter(description = STUDENT_ID_DESCR) long studentId,
                           @PathVariable("subjectId") @Parameter(description = SUBJECT_ID_DESCR) long subjectId)
            throws NotFoundException {
        Student student = studentService.get(studentId);
        Subject subject = subjectService.get(student, subjectId);
        return mappingHelper.mapBeanToDto(subject);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(summary = "Добавить новый предмет студенту")
    @ApiResponse(responseCode = "201", description = HTTP_201_DESCR)
    @ApiResponse(responseCode = "400", description = HTTP_400_DESCR, content = @Content)
    @ApiResponse(responseCode = "404", description = HTTP_404_STUDENT_DESCR, content = @Content)
    @ApiResponse(responseCode = "409", description = HTTP_409_DESCR, content = @Content)
    @Transactional
    public SubjectDto create(@PathVariable("studentId") @Parameter(description = STUDENT_ID_DESCR) long studentId,
                             @Valid @RequestBody SubjectDto subjectDto) throws NotFoundException, AlreadyExistsException {
        Student student = studentService.get(studentId);
        Subject subject = subjectService.create(student, subjectDto);
        return mappingHelper.mapBeanToDto(subject);
    }

    @PutMapping("/{subjectId}")
    @Operation(summary = "Изменить информайию о предмете")
    @ApiResponse(responseCode = "200", description = HTTP_200_DESCR)
    @ApiResponse(responseCode = "400", description = HTTP_400_DESCR, content = @Content)
    @ApiResponse(responseCode = "404", description = HTTP_404_STUDENT_OR_SUBJECT_DESCR, content = @Content)
    @ApiResponse(responseCode = "409", description = HTTP_409_DESCR, content = @Content)
    @Transactional
    public SubjectDto update(@PathVariable("studentId") @Parameter(description = STUDENT_ID_DESCR) long studentId,
                             @PathVariable("subjectId") @Parameter(description = SUBJECT_ID_DESCR) long subjectId,
                             @Valid @RequestBody SubjectDto subjectDto) throws NotFoundException, AlreadyExistsException {
        Student student = studentService.get(studentId);
        Subject subject = subjectService.update(student, subjectId, subjectDto);
        return mappingHelper.mapBeanToDto(subject);
    }

    @DeleteMapping("/{subjectId}")
    @Operation(summary = "Удалить предмент у студента")
    @ApiResponse(responseCode = "200", description = HTTP_200_DESCR)
    @ApiResponse(responseCode = "404", description = HTTP_404_STUDENT_OR_SUBJECT_DESCR, content = @Content)
    @Transactional
    public void delete(@PathVariable("studentId") @Parameter(description = STUDENT_ID_DESCR) long studentId,
                       @PathVariable("subjectId") @Parameter(description = SUBJECT_ID_DESCR) long subjectId)
            throws NotFoundException {
        Student student = studentService.get(studentId);
        subjectService.delete(student, subjectId);
    }
}

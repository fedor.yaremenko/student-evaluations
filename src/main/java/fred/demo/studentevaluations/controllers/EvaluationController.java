package fred.demo.studentevaluations.controllers;

import fred.demo.studentevaluations.dtos.AverageEvaluationDto;
import fred.demo.studentevaluations.dtos.SubjectDto;
import fred.demo.studentevaluations.dtos.UpdateEvaluationDto;
import fred.demo.studentevaluations.exceptions.NotFoundException;
import fred.demo.studentevaluations.mapping.MappingHelper;
import fred.demo.studentevaluations.model.Student;
import fred.demo.studentevaluations.model.Subject;
import fred.demo.studentevaluations.services.EvaluationService;
import fred.demo.studentevaluations.services.StudentService;
import fred.demo.studentevaluations.services.SubjectService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static fred.demo.studentevaluations.Constants.*;

@RestController
@Tag(name = "Оценки по предметам", description = "Управление оценками по предметам")
public class EvaluationController {

    private final static String HTTP_200_DESCR = "OK";
    private final static String HTTP_400_DESCR = "Неправильно сформированный запрос";
    private final static String HTTP_404_STUDENT_DESCR = "Студент не найден";
    private final static String HTTP_404_STUDENT_OR_SUBJECT_DESCR = "Студент или предмет не найден";

    private final static String STUDENT_ID_DESCR = "Идентификатор студента";
    private final static String SUBJECT_ID_DESCR = "Идентификатор предмета";

    private final MappingHelper mappingHelper;
    private final StudentService studentService;
    private final SubjectService subjectService;
    private final EvaluationService evaluationService;

    public EvaluationController(MappingHelper mappingHelper,
                                StudentService studentService,
                                SubjectService subjectService,
                                EvaluationService evaluationService) {
        this.mappingHelper = mappingHelper;
        this.studentService = studentService;
        this.subjectService = subjectService;
        this.evaluationService = evaluationService;
    }

    @PutMapping(SUBJECTS_PATH + "/{subjectId}" + UPDATE_EVALUATION_SUBPATH)
    @Operation(summary = "Поставить оценку по предмету")
    @ApiResponse(responseCode = "200", description = HTTP_200_DESCR)
    @ApiResponse(responseCode = "400", description = HTTP_400_DESCR, content = @Content)
    @ApiResponse(responseCode = "404", description = HTTP_404_STUDENT_OR_SUBJECT_DESCR, content = @Content)
    @Transactional
    public SubjectDto updateEvaluation(@PathVariable("studentId") @Parameter(description = STUDENT_ID_DESCR) long studentId,
                                       @PathVariable("subjectId") @Parameter(description = SUBJECT_ID_DESCR) long subjectId,
                                       @Valid @RequestBody UpdateEvaluationDto updateEvaluationDto) throws NotFoundException {
        Student student = studentService.get(studentId);
        Subject subject = subjectService.get(student, subjectId);
        evaluationService.updateEvaluation(subject, updateEvaluationDto);
        return mappingHelper.mapBeanToDto(subject);
    }

    @GetMapping(STUDENTS_PATH + "/{studentId}" + AVG_EVALUATION_SUBPATH)
    @ApiResponse(responseCode = "200", description = HTTP_200_DESCR)
    @ApiResponse(responseCode = "404", description = HTTP_404_STUDENT_DESCR, content = @Content)
    @Transactional(readOnly = true)
    public AverageEvaluationDto getAverageEvaluation(@PathVariable("studentId") @Parameter(description = STUDENT_ID_DESCR) long studentId)
            throws NotFoundException {
        Student student = studentService.get(studentId);
        return evaluationService.getAverageEvaluation(student);
    }

}

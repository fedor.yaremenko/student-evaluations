package fred.demo.studentevaluations.controllers;

import fred.demo.studentevaluations.dtos.StudentDto;
import fred.demo.studentevaluations.exceptions.AlreadyExistsException;
import fred.demo.studentevaluations.exceptions.NotFoundException;
import fred.demo.studentevaluations.mapping.MappingHelper;
import fred.demo.studentevaluations.model.Student;
import fred.demo.studentevaluations.services.StudentService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;
import java.util.stream.Collectors;

import static fred.demo.studentevaluations.Constants.STUDENTS_PATH;

@RestController
@RequestMapping(STUDENTS_PATH)
@Tag(name = "Студенты", description = "Управление студентами")
public class StudentController {

    private final static String HTTP_200_DESCR = "OK";
    private final static String HTTP_201_DESCR = "Студент создан";
    private final static String HTTP_400_DESCR = "Неправильно сформированный запрос";
    private final static String HTTP_404_DESCR = "Студент не найден";
    private final static String HTTP_409_DESCR = "Студент с заданными ФИО уже есть в системе";

    private final static String ID_DESCR = "Идентификатор студента";

    private final MappingHelper mappingHelper;
    private final StudentService studentService;

    public StudentController(MappingHelper mappingHelper, StudentService studentService) {
        this.mappingHelper = mappingHelper;
        this.studentService = studentService;
    }

    @GetMapping
    @Operation(summary = "Получить список всех студентов")
    @ApiResponse(responseCode = "200", description = HTTP_200_DESCR)
    @Transactional(readOnly = true)
    public List<StudentDto> list() {
        return studentService.findAll().stream()
                .map(mappingHelper::mapBeanToDto)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    @Operation(summary = "Получить информацию о студенте")
    @ApiResponse(responseCode = "200", description = HTTP_200_DESCR)
    @ApiResponse(responseCode = "404", description = HTTP_404_DESCR, content = @Content)
    @Transactional(readOnly = true)
    public StudentDto read(@PathVariable("id") @Parameter(description = ID_DESCR) long id) throws NotFoundException {
        Student student = studentService.get(id);
        return mappingHelper.mapBeanToDto(student);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(summary = "Создать нового студента")
    @ApiResponse(responseCode = "201", description = HTTP_201_DESCR)
    @ApiResponse(responseCode = "400", description = HTTP_400_DESCR, content = @Content)
    @ApiResponse(responseCode = "409", description = HTTP_409_DESCR, content = @Content)
    @Transactional
    public StudentDto create(@Valid @RequestBody StudentDto studentDto) throws AlreadyExistsException {
        Student student = studentService.create(studentDto);
        return mappingHelper.mapBeanToDto(student);
    }

    @PutMapping("/{id}")
    @Operation(summary = "Изменение информации о студенте")
    @ApiResponse(responseCode = "200", description = HTTP_200_DESCR)
    @ApiResponse(responseCode = "400", description = HTTP_400_DESCR, content = @Content)
    @ApiResponse(responseCode = "404", description = HTTP_404_DESCR, content = @Content)
    @ApiResponse(responseCode = "409", description = HTTP_409_DESCR, content = @Content)
    @Transactional
    public StudentDto update(@PathVariable("id") @Parameter(description = ID_DESCR) long id,
                             @Valid @RequestBody StudentDto studentDto) throws NotFoundException, AlreadyExistsException {
        Student student = studentService.update(id, studentDto);
        return mappingHelper.mapBeanToDto(student);
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Удаление студента")
    @ApiResponse(responseCode = "200", description = HTTP_200_DESCR)
    @ApiResponse(responseCode = "404", description = HTTP_404_DESCR, content = @Content)
    @Transactional
    public void delete(@PathVariable("id") @Parameter(description = ID_DESCR) long id) throws NotFoundException {
        studentService.delete(id);
    }
}

package fred.demo.studentevaluations.mapping;

import fred.demo.studentevaluations.dtos.StudentDto;
import fred.demo.studentevaluations.dtos.SubjectDto;
import fred.demo.studentevaluations.dtos.UpdateEvaluationDto;
import fred.demo.studentevaluations.model.Student;
import fred.demo.studentevaluations.model.Subject;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class MappingHelper {

    private final ModelMapper modelMapper;

    public MappingHelper() {
        modelMapper = new ModelMapper();
        modelMapper.typeMap(StudentDto.class, Student.class).addMappings(mapper -> mapper.skip(Student::setId));
        modelMapper.typeMap(SubjectDto.class, Subject.class).addMappings(mapper -> {
            mapper.skip(Subject::setId);
            mapper.skip(Subject::setEvaluation);
        });
    }

    // Students

    public Student mapDtoToBean(StudentDto studentDto) {
        return modelMapper.map(studentDto, Student.class);
    }

    public void mapDtoToBean(StudentDto studentDto, Student student) {
        modelMapper.map(studentDto, student);
    }

    public StudentDto mapBeanToDto(Student student) {
        return modelMapper.map(student, StudentDto.class);
    }

    // Subjects

    public Subject mapDtoToBean(SubjectDto subjectDto) {
        return modelMapper.map(subjectDto, Subject.class);
    }

    public void mapDtoToBean(SubjectDto subjectDto, Subject subject) {
        modelMapper.map(subjectDto, subject);
    }

    public SubjectDto mapBeanToDto(Subject subject) {
        return modelMapper.map(subject, SubjectDto.class);
    }

    // Evaluations

    public void mapDtoToBean(UpdateEvaluationDto updateEvaluationDto, Subject subject) {
        modelMapper.map(updateEvaluationDto, subject);
    }
}

drop table if exists subjects;
drop table if exists students;

-- Students
create table students(
    id long auto_increment,
    last_name varchar(255) not null,
    first_name varchar(255) not null,
    sur_name varchar(255) not null
);

alter table students add constraint pk_students primary key(id);
alter table students add constraint unq_students unique(last_name, first_name, sur_name);

-- Subjects
create table subjects(
    id long auto_increment,
    student_id long not null,
    title varchar(255) not null,
    evaluation int
);

alter table subjects add constraint pk_subjects primary key(id);
alter table subjects add constraint fk_subject__student_id foreign key(student_id) references students;
alter table subjects add constraint unq_subjects unique(student_id, title);
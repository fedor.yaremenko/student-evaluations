package fred.demo.studentevaluations.controllers;

import fred.demo.studentevaluations.model.Student;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import java.util.List;

import static fred.demo.studentevaluations.Constants.STUDENTS_PATH;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

@DisplayName("Интеграционные тесты контроллера студентов")
public class StudentControllerTest extends BaseControllerTest {

    private boolean checkStudentName(Student student, String lastName, String firstName, String surName) {
        return student.getLastName().equals(lastName) &&
                student.getFirstName().equals(firstName) &&
                student.getSurName().equals(surName);
    }

    // list

    @Test
    @DisplayName("Должен возвращать список студентов")
    public void shouldReturnListOfStudents() throws Exception {
        givenIvanov();
        givenPetrov();
        assertRestCall(get(STUDENTS_PATH), HttpStatus.OK, "studentList.json");
    }

    // read

    @Test
    @DisplayName("Должен возвращать информацию о студенте по id")
    public void shouldReturnStudent() throws Exception {
        Student ivanov = givenIvanov();
        assertRestCall(get(STUDENTS_PATH + "/{id}", ivanov.getId()), HttpStatus.OK, "ivanovWithId.json");
    }

    @Test
    @DisplayName("Должен возвращать ошибку 404 при попытке получения информации о несуществующем студенте")
    public void shouldNotReturnNotFoundStudent() throws Exception {
        assertRestCall(get(STUDENTS_PATH + "/{id}", 1), HttpStatus.NOT_FOUND);
    }

    // create

    @Test
    @DisplayName("Должен создавать студента")
    public void shouldCreateStudent() throws Exception {
        assertRestCall(post(STUDENTS_PATH), "ivanov.json", HttpStatus.CREATED, "ivanovWithId.json");

        List<Student> students = studentRepository.findAll();
        assertThat(students).hasSize(1).element(0)
                .matches(student -> checkStudentName(student, "Иванов", "Иван", "Иванович"));
    }

    @Test
    @DisplayName("Должен возвращать ошибку 409 при попытке создать студента с дублирующимся именем")
    public void shouldNotCreateStudentWithDuplicatedName() throws Exception {
        givenIvanov();
        assertRestCall(post(STUDENTS_PATH), "ivanov.json", HttpStatus.CONFLICT);
    }

    // update

    @Test
    @DisplayName("Должен обновлять информацию о студенте")
    public void shouldUpdateStudent() throws Exception {
        long studentId = givenPetrov().getId();
        assertRestCall(put(STUDENTS_PATH + "/{id}", studentId), "ivanov.json", HttpStatus.OK, "ivanovWithId.json");

        assertThat(studentRepository.findById(studentId).orElseThrow())
                .matches(student -> checkStudentName(student, "Иванов", "Иван", "Иванович"));

    }

    @Test
    @DisplayName("Должен обновлять информацию о студенте если имя не меняется")
    // Проверяется, что не будет ошибки 409
    public void shouldUpdateStudentWithoutModifications() throws Exception {
        long studentId = givenIvanov().getId();
        assertRestCall(put(STUDENTS_PATH + "/{id}", studentId), "ivanov.json", HttpStatus.OK, "ivanovWithId.json");

        assertThat(studentRepository.findById(studentId).orElseThrow())
                .matches(student -> checkStudentName(student, "Иванов", "Иван", "Иванович"));

    }

    @Test
    @DisplayName("Должен возвращать ошибку 404 при попытке обновить информацию о несуществующем студенте")
    public void shouldNotUpdateNotFound() throws Exception {
        assertRestCall(put(STUDENTS_PATH + "/{id}", 1), "ivanov.json", HttpStatus.NOT_FOUND);
    }

    @Test
    @DisplayName("Должен возвращать ошибку 409 при попытке задать студенту дублируещееся имя")
    public void shouldNotUpdateStudentWithDuplicatedName() throws Exception {
        givenIvanov();
        long studentId = givenPetrov().getId();
        assertRestCall(put(STUDENTS_PATH + "/{id}", studentId), "ivanov.json", HttpStatus.CONFLICT);
    }

    // delete

    @Test
    @DisplayName("Должен удалять студента")
    public void shouldDeleteStudent() throws Exception {
        long id = givenIvanov().getId();
        assertRestCall(delete(STUDENTS_PATH + "/{id}", id), HttpStatus.OK);

        assertThat(studentRepository.findById(id).isPresent()).isFalse();
    }

    @Test
    @DisplayName("Должен возвращать ошибку 404 при попытке удалить несуществующего студента")
    public void shouldNotDeleteNotFoundStudent() throws Exception {
        assertRestCall(delete(STUDENTS_PATH + "/{id}", 1), HttpStatus.NOT_FOUND);
    }
}


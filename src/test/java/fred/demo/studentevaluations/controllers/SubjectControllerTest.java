package fred.demo.studentevaluations.controllers;

import fred.demo.studentevaluations.model.Student;
import fred.demo.studentevaluations.model.Subject;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import java.util.List;

import static fred.demo.studentevaluations.Constants.SUBJECTS_PATH;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@DisplayName("Интеграционные тесты контроллера предметов")
public class SubjectControllerTest extends BaseControllerTest {

    // list

    @Test
    @DisplayName("Должен возвращать список предметов студента")
    public void shouldReturnListOfSubjectsForStudent() throws Exception {
        Student student = givenIvanov();
        addFunctionalAnalysis(student);
        addEnglish(student);

        assertRestCall(get(SUBJECTS_PATH, student.getId()), HttpStatus.OK, "subjectsList.json");
    }

    @Test
    @DisplayName("Должен возвращать ошибку 404 при попытке получения списка предметов несуществующего студента")
    public void shouldNotReturnListOfSubjectsForNotFoundStudent() throws Exception {
        assertRestCall(get(SUBJECTS_PATH, 1), HttpStatus.NOT_FOUND);
    }

    // read

    @Test
    @DisplayName("Должен возвращать информацию о предмете")
    public void shouldReturnSubject() throws Exception {
        Student student = givenIvanov();
        long subjectId = addFunctionalAnalysis(student).getId();
        assertRestCall(get(SUBJECTS_PATH + "/{subjectId}", student.getId(), subjectId),
                HttpStatus.OK, "functionalAnalysisWithIdAndEvaluation.json");
    }

    @Test
    @DisplayName("Должен возвращать ошибку 404 при попытке получения предмета у несуществующего студента")
    public void shouldNotReturnSubjectForNotFoundStudent() throws Exception {
        Student student = givenIvanov();
        long subjectId = addFunctionalAnalysis(student).getId();
        assertRestCall(get(SUBJECTS_PATH + "/{subjectId}", student.getId() + 1, subjectId), HttpStatus.NOT_FOUND);
    }

    @Test
    @DisplayName("Должен возвращать ошибку 404 при попытке получения предмета у другого студента")
    public void shouldNotReturnSubjectForAnotherStudent() throws Exception {
        Student ivanov = givenIvanov();
        Student petrov = givenPetrov();
        long subjectId = addFunctionalAnalysis(petrov).getId();
        assertRestCall(get(SUBJECTS_PATH + "/{subjectId}", ivanov.getId(), subjectId), HttpStatus.NOT_FOUND);
    }

    @Test
    @DisplayName("Должен возвращать ошибку 404 при попытке получения несуществующего предмета")
    public void shouldNotReturnNotFoundSubject() throws Exception {
        Student ivanov = givenIvanov();
        assertRestCall(get(SUBJECTS_PATH + "/{subjectId}", ivanov.getId(), 1), HttpStatus.NOT_FOUND);
    }

    // create

    @Test
    @DisplayName("Должен создавать предмет, в том числе если у другого студента есть предмет с таким же названием")
    public void shouldCreateSubject() throws Exception {
        Student ivanov = givenIvanov();
        Student petrov = givenPetrov();
        addFunctionalAnalysis(petrov);
        assertRestCall(post(SUBJECTS_PATH, ivanov.getId()), "functionalAnalysis.json",
                HttpStatus.CREATED, "functionalAnalysisWithId.json");

        List<Subject> subjects = subjectRepository.findByStudent(ivanov);
        assertThat(subjects).hasSize(1).element(0)
                .matches(subject -> "Функциональный анализ".equals(subject.getTitle()));
    }

    @Test
    @DisplayName("Должен возвращать ошибку 404 при попытке получения предмета у несуществующего студента")
    public void shouldNotCreateSubjectForNotFoundStudent() throws Exception {
        assertRestCall(post(SUBJECTS_PATH, 1), "functionalAnalysis.json", HttpStatus.NOT_FOUND);
    }

    @Test
    @DisplayName("Должен возвращать ошибку 409 при попытке создать у студента предмет с дублируещеемся названием")
    public void shouldNotCreateDuplicatedSubject() throws Exception {
        Student ivanov = givenIvanov();
        addFunctionalAnalysis(ivanov);
        assertRestCall(post(SUBJECTS_PATH, ivanov.getId()), "functionalAnalysis.json", HttpStatus.CONFLICT);
    }

    // update

    @Test
    @DisplayName("Должен обновлять предмет, в том числе если у другого студента есть предмет с таким же названием")
    public void shouldUpdateSubject() throws Exception {
        Student ivanov = givenIvanov();
        long subjectId = addEnglish(ivanov).getId();

        Student petrov = givenPetrov();
        addFunctionalAnalysis(petrov);

        assertRestCall(put(SUBJECTS_PATH + "/{subjectId}", ivanov.getId(), subjectId), "functionalAnalysis.json",
                HttpStatus.OK, "functionalAnalysisWithIdAndEvaluation5.json");

        Subject subject = subjectRepository.findById(subjectId).orElseThrow();
        assertThat(subject.getTitle()).isEqualTo("Функциональный анализ");
        assertThat(subject.getEvaluation()).isEqualTo(5);
    }

    @Test
    @DisplayName("Должен обновлять предмет если название не меняется")
    // Проверяется, что не будет ошибки 409
    public void shouldUpdateSubjectWithoutModifications() throws Exception {
        Student ivanov = givenIvanov();
        long subjectId = addFunctionalAnalysis(ivanov).getId();

        assertRestCall(put(SUBJECTS_PATH + "/{subjectId}", ivanov.getId(), subjectId), "functionalAnalysis.json",
                HttpStatus.OK, "functionalAnalysisWithIdAndEvaluation.json");
    }

    @Test
    @DisplayName("Должен возвращать ошибку 404 при попытке обновить предмет у несуществующего студента")
    public void shouldNotUpdateSubjectForNotFoundStudent() throws Exception {
        Student ivanov = givenIvanov();
        long subjectId = addFunctionalAnalysis(ivanov).getId();
        assertRestCall(put(SUBJECTS_PATH + "/{subjectId}", ivanov.getId() + 1, subjectId), "functionalAnalysis.json",
                HttpStatus.NOT_FOUND);
    }

    @Test
    @DisplayName("Должен возвращать ошибку 404 при попытке обновить несуществующий предмет")
    public void shouldNotUpdateNotFoundSubject() throws Exception {
        Student ivanov = givenIvanov();
        assertRestCall(put(SUBJECTS_PATH + "/{subjectId}", ivanov.getId(), 1), "functionalAnalysis.json",
                HttpStatus.NOT_FOUND);
    }

    @Test
    @DisplayName("Должен возвращать ошибку 409 при попытке задать предмету дублируещееся название")
    public void shouldNotUpdateDuplicatedSubject() throws Exception {
        Student ivanov = givenIvanov();
        addFunctionalAnalysis(ivanov);
        long subjectId = addEnglish(ivanov).getId();
        assertRestCall(put(SUBJECTS_PATH + "/{subjectId}", ivanov.getId(), subjectId), "functionalAnalysis.json",
                HttpStatus.CONFLICT);
    }

    // delete

    @Test
    @DisplayName("Должен удалять предмет")
    public void shouldDeleteSubject() throws Exception {
        Student ivanov = givenIvanov();
        long subjectId = addFunctionalAnalysis(ivanov).getId();
        assertRestCall(delete(SUBJECTS_PATH + "/{subjectId}", ivanov.getId(), subjectId), HttpStatus.OK);

        List<Subject> subjects = subjectRepository.findByStudent(ivanov);
        assertThat(subjects).isEmpty();
    }

    @Test
    @DisplayName("Должен возвращать ошибку 404 при попытке удалить предмет у несуществующего студента")
    public void shouldNotDeleteSubjectForNotFoundStudent() throws Exception {
        Student ivanov = givenIvanov();
        long subjectId = addFunctionalAnalysis(ivanov).getId();
        assertRestCall(delete(SUBJECTS_PATH + "/{subjectId}", ivanov.getId() + 1, subjectId), HttpStatus.NOT_FOUND);
    }

    @Test
    @DisplayName("Должен возвращать ошибку 404 при попытке удалить предмет у другого студента")
    public void shouldNotDeleteSubjectForAnotherStudent() throws Exception {
        Student ivanov = givenIvanov();
        Student petrov = givenPetrov();
        long subjectId = addFunctionalAnalysis(petrov).getId();
        assertRestCall(delete(SUBJECTS_PATH + "/{subjectId}", ivanov.getId(), subjectId), HttpStatus.NOT_FOUND);
    }

    @Test
    @DisplayName("Должен возвращать ошибку 404 при попытке удалить несуществующий предмет")
    public void shouldNotDeleteNotFoundSubject() throws Exception {
        Student ivanov = givenIvanov();
        assertRestCall(put(SUBJECTS_PATH + "/{subjectId}", ivanov.getId(), 1), "functionalAnalysis.json",
                HttpStatus.NOT_FOUND);
    }
}

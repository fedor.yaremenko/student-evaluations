package fred.demo.studentevaluations.controllers;

import fred.demo.studentevaluations.model.Student;
import fred.demo.studentevaluations.model.Subject;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import static fred.demo.studentevaluations.Constants.*;
import static fred.demo.studentevaluations.Constants.UPDATE_EVALUATION_SUBPATH;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

@DisplayName("Интеграционные тесты оценок по предметам")
public class EvaluationControllerTest extends BaseControllerTest {

    // update evaluation

    @Test
    @DisplayName("Должен обновлять оценку по предмету")
    public void shouldUpdateEvaluation() throws Exception {
        Student ivanov = givenIvanov();
        Long subjectId = addFunctionalAnalysis(ivanov).getId();
        assertRestCall(put(SUBJECTS_PATH + "/{subjectId}" + UPDATE_EVALUATION_SUBPATH, ivanov.getId(), subjectId),
                "updateEvaluation.json", HttpStatus.OK, "functionalAnalysisWithIdAndEvaluation5.json");

        Subject subject = subjectRepository.findById(subjectId).orElseThrow();
        assertThat(subject.getEvaluation()).isEqualTo(5);
    }

    @Test
    @DisplayName("Должен возвращать ошибку 404 при попытке обновить оценку у несуществующего студента")
    public void shouldNotUpdateEvaluationForNotFoundStudent() throws Exception {
        Student ivanov = givenIvanov();
        Long subjectId = addFunctionalAnalysis(ivanov).getId();
        assertRestCall(put(SUBJECTS_PATH + "/{subjectId}" + UPDATE_EVALUATION_SUBPATH, ivanov.getId() + 1, subjectId),
                "updateEvaluation.json", HttpStatus.NOT_FOUND);
    }

    @Test
    @DisplayName("Должен возвращать ошибку 404 при попытке обновить оценку у другого студента")
    public void shouldNotUpdateEvaluationForAnotherStudent() throws Exception {
        Student ivanov = givenIvanov();
        Student petrov = givenPetrov();
        Long subjectId = addFunctionalAnalysis(petrov).getId();
        assertRestCall(put(SUBJECTS_PATH + "/{subjectId}" + UPDATE_EVALUATION_SUBPATH, ivanov.getId(), subjectId),
                "updateEvaluation.json", HttpStatus.NOT_FOUND);
    }

    @Test
    @DisplayName("Должен возвращать ошибку 404 при попытке обновить оценку по несуществующему предмету")
    public void shouldNotUpdateEvaluationForNotFoundSubject() throws Exception {
        Student ivanov = givenIvanov();
        assertRestCall(put(SUBJECTS_PATH + "/{subjectId}" + UPDATE_EVALUATION_SUBPATH, ivanov.getId(), 1),
                "updateEvaluation.json", HttpStatus.NOT_FOUND);
    }

    // average evaluation

    @Test
    @DisplayName("Должен возвращать среднюю оценку null для студента без оценок")
    public void shouldReturnNullAverageEvaluation() throws Exception {
        long studentId = givenIvanov().getId();
        assertRestCall(get(STUDENTS_PATH + "/{studentId}" + AVG_EVALUATION_SUBPATH, studentId),
                HttpStatus.OK, "avgEvaluationNull.json");
    }

    @Test
    @DisplayName("Должен возвращать среднюю оценку null для студента")
    public void shouldReturnAverageEvaluation() throws Exception {
        Student ivanov = givenIvanov();
        addFunctionalAnalysis(ivanov);
        addEnglish(ivanov);
        addSubject(ivanov, "Линейная алгебра", null);

        assertRestCall(get(STUDENTS_PATH + "/{studentId}" + AVG_EVALUATION_SUBPATH, ivanov.getId()),
                HttpStatus.OK, "avgEvaluation.json");
    }

    @Test
    @DisplayName("Должен возвращать ошибку 404 при попытке получения среднуюю оценку у несуществующего студента")
    public void shouldNotReturnAverageEvaluationNotFoundStudent() throws Exception {
        assertRestCall(get(STUDENTS_PATH + "/{studentId}" + AVG_EVALUATION_SUBPATH, 1), HttpStatus.NOT_FOUND);
    }
}

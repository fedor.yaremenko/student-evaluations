package fred.demo.studentevaluations.controllers;

import fred.demo.studentevaluations.model.Student;
import fred.demo.studentevaluations.model.Subject;
import fred.demo.studentevaluations.repositories.StudentRepository;
import fred.demo.studentevaluations.repositories.SubjectRepository;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import static net.javacrumbs.jsonunit.core.Option.IGNORING_ARRAY_ORDER;
import static net.javacrumbs.jsonunit.spring.JsonUnitResultMatchers.json;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@Tag("integration")
public class BaseControllerTest {

    @Autowired
    protected MockMvc mockMvc;

    @Autowired
    protected StudentRepository studentRepository;

    @Autowired
    protected SubjectRepository subjectRepository;

    private String getJsonResourceAsString(String fileName) throws Exception {
        try (InputStream resourceStream = getClass().getResourceAsStream(fileName)) {
            return IOUtils.toString(resourceStream, StandardCharsets.UTF_8);
        }
    }

    protected void assertRestCall(MockHttpServletRequestBuilder requestBuilder,
                                String requestJsonFileName,
                                HttpStatus expectedStatus,
                                String expectedResponseJsonFileName) throws Exception {
        if (requestJsonFileName != null) {
            requestBuilder.content(getJsonResourceAsString(requestJsonFileName)).contentType(MediaType.APPLICATION_JSON);
        }
        ResultActions resultActions = mockMvc.perform(requestBuilder).andExpect(status().is(expectedStatus.value()));
        if (expectedResponseJsonFileName != null) {
            resultActions.andExpect(json().when(IGNORING_ARRAY_ORDER)
                    .isEqualTo(getJsonResourceAsString(expectedResponseJsonFileName)));
        }
    }

    protected void assertRestCall(MockHttpServletRequestBuilder requestBuilder,
                                String requestJsonFileName,
                                HttpStatus expectedStatus) throws Exception {
        assertRestCall(requestBuilder, requestJsonFileName, expectedStatus, null);
    }

    protected void assertRestCall(MockHttpServletRequestBuilder requestBuilder,
                                HttpStatus expectedStatus,
                                String expectedResponseJsonFileName) throws Exception {
        assertRestCall(requestBuilder, null, expectedStatus, expectedResponseJsonFileName);
    }

    protected void assertRestCall(MockHttpServletRequestBuilder requestBuilder,
                                HttpStatus expectedStatus) throws Exception {
        assertRestCall(requestBuilder, null, expectedStatus, null);
    }

    protected Student givenIvanov() {
        Student ivanov = Student.of("Иванов", "Иван", "Иванович");
        studentRepository.save(ivanov);
        return ivanov;
    }

    protected Student givenPetrov() {
        Student petrov = Student.of("Петров", "Петр", "Петрович");
        studentRepository.save(petrov);
        return petrov;
    }

    protected Subject addSubject(Student student, String title, Integer evaluation) {
        Subject subject = Subject.of(student, title, evaluation);
        subjectRepository.save(subject);
        return subject;
    }

    protected Subject addFunctionalAnalysis(Student student) {
        return addSubject(student, "Функциональный анализ", 10);
    }

    protected Subject addEnglish(Student student) {
        return addSubject(student, "Английский язык", 5);
    }
}

package fred.demo.studentevaluations.mapping;

import fred.demo.studentevaluations.dtos.StudentDto;
import fred.demo.studentevaluations.dtos.SubjectDto;
import fred.demo.studentevaluations.dtos.UpdateEvaluationDto;
import fred.demo.studentevaluations.model.Student;
import fred.demo.studentevaluations.model.Subject;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

@DisplayName("Юнит-тесты маппинга")
@Tag("unit")
public class MappingHelperTest {

    private final MappingHelper mappingHelper = new MappingHelper();

    // students

    private Student givenIvanov() {
        return Student.of("Иванов", "Иван", "Иванович");
    }

    private Student givenPetrov() {
        return Student.of("Петров", "Петр", "Петрович");
    }

    private StudentDto givenIvanovDto() {
        StudentDto studentDto = new StudentDto();
        studentDto.setLastName("Иванов");
        studentDto.setFirstName("Иван");
        studentDto.setSurName("Иванович");
        return studentDto;
    }

    @Test
    @DisplayName("Должен замаппить DTO студента в новый объект")
    public void shouldMapStudentDtoToNewBean() {
        Student actualStudent = mappingHelper.mapDtoToBean(givenIvanovDto());
        Student expectedStudent = givenIvanov();

        assertThat(actualStudent).usingRecursiveComparison().isEqualTo(expectedStudent);
    }

    @Test
    @DisplayName("Должен замаппить DTO студента в объект")
    public void shouldMapStudentDtoToBean() {
        Student actualStudent = givenPetrov();
        actualStudent.setId(1L);
        actualStudent.addSubject(new Subject());
        mappingHelper.mapDtoToBean(givenIvanovDto(), actualStudent);

        Student expectedStudent = givenIvanov();
        expectedStudent.setId(1L);
        expectedStudent.addSubject(new Subject());

        assertThat(actualStudent).usingRecursiveComparison().isEqualTo(expectedStudent);
    }

    @Test
    @DisplayName("Должен замаппить объект студента в DTO")
    public void shouldMapStudentBeanToDto() {
        Student student = givenIvanov();
        student.setId(1L);
        student.addSubject(new Subject());
        StudentDto actualStudentDto = mappingHelper.mapBeanToDto(student);

        StudentDto expectedStudentDto = givenIvanovDto();
        expectedStudentDto.setId(1L);

        assertThat(actualStudentDto).usingRecursiveComparison().isEqualTo(expectedStudentDto);
    }

    // subjects

    private Subject givenFunctionalAnalysis() {
        Subject functionalAnalysis = new Subject();
        functionalAnalysis.setTitle("Функциональный анализ");
        return functionalAnalysis;
    }

    private Subject givenEnglish() {
        Subject english = new Subject();
        english.setTitle("Английский язык");
        return english;
    }

    private SubjectDto givenFunctionalAnalysisDto() {
        SubjectDto functionalAnalysisDto = new SubjectDto();
        functionalAnalysisDto.setTitle("Функциональный анализ");
        return functionalAnalysisDto;
    }

    @Test
    @DisplayName("Должен замаппить DTO предмета в новый объект")
    public void shouldMapSubjectDtoToNewBean() {
        Subject actualSubject = mappingHelper.mapDtoToBean(givenFunctionalAnalysisDto());

        Subject expectedSubject = givenFunctionalAnalysis();

        assertThat(actualSubject).usingRecursiveComparison().isEqualTo(expectedSubject);
    }

    @Test
    @DisplayName("Должен замаппить DTO предмета в объект")
    public void shouldMapSubjectDtoToBean() {
        Subject actualSubject = givenEnglish();
        actualSubject.setId(1L);
        actualSubject.setEvaluation(5);
        actualSubject.setStudent(new Student());
        mappingHelper.mapDtoToBean(givenFunctionalAnalysisDto(), actualSubject);

        Subject expectedSubject = givenFunctionalAnalysis();
        expectedSubject.setId(1L);
        expectedSubject.setEvaluation(5);
        expectedSubject.setStudent(new Student());

        assertThat(actualSubject).usingRecursiveComparison().isEqualTo(expectedSubject);
    }

    @Test
    @DisplayName("Должен замаппить объект предмета в DTO")
    public void shouldMapSubjectBeanToDto() {
        Subject subject = givenFunctionalAnalysis();
        subject.setId(1L);
        subject.setEvaluation(5);
        subject.setStudent(new Student());
        SubjectDto actualSubjectDto = mappingHelper.mapBeanToDto(subject);

        SubjectDto expectedSubjectDto = givenFunctionalAnalysisDto();
        expectedSubjectDto.setId(1L);
        expectedSubjectDto.setEvaluation(5);

        assertThat(actualSubjectDto).usingRecursiveComparison().isEqualTo(expectedSubjectDto);
    }

    // evaluations

    @Test
    @DisplayName("Должен замаппить DTO оценки в объект предмета")
    public void shouldMapEvaluationDtoToBean() {
        Subject actualSubject = givenFunctionalAnalysis();
        actualSubject.setId(1L);
        actualSubject.setEvaluation(5);
        actualSubject.setStudent(new Student());

        UpdateEvaluationDto updateEvaluationDto = new UpdateEvaluationDto();
        updateEvaluationDto.setEvaluation(10);
        mappingHelper.mapDtoToBean(updateEvaluationDto, actualSubject);

        Subject expectedSubject = givenFunctionalAnalysis();
        expectedSubject.setId(1L);
        expectedSubject.setEvaluation(10);
        expectedSubject.setStudent(new Student());

        assertThat(actualSubject).usingRecursiveComparison().isEqualTo(expectedSubject);
    }
}

package fred.demo.studentevaluations.services;

import fred.demo.studentevaluations.dtos.SubjectDto;
import fred.demo.studentevaluations.exceptions.AlreadyExistsException;
import fred.demo.studentevaluations.exceptions.NotFoundException;
import fred.demo.studentevaluations.mapping.MappingHelper;
import fred.demo.studentevaluations.model.Student;
import fred.demo.studentevaluations.model.Subject;
import fred.demo.studentevaluations.repositories.SubjectRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.given;

@DisplayName("Юнит-тесты сервиса управления предметами студентов")
@Tag("unit")
public class SubjectServiceTest {

    private SubjectRepository subjectRepository;
    private SubjectService subjectService;

    private Subject givenSubject() {
        Student student = new Student();
        Subject subject = Subject.of(student, "subject", null);
        subject.setId(1L);
        return subject;
    }

    @BeforeEach
    void setUp() {
        subjectRepository = Mockito.mock(SubjectRepository.class);
        MappingHelper mappingHelper = new MappingHelper();
        subjectService = new SubjectService(mappingHelper, subjectRepository);
    }

    // list

    @Test
    @DisplayName("Должен вернуть список предметов студента")
    public void shouldReturnSubjectList() {
        given(subjectRepository.findByStudent(any())).willReturn(List.of(new Subject(), new Subject()));

        assertThat(subjectService.findAll(new Student())).hasSize(2);
    }

    // read

    @Test
    @DisplayName("Должен найти предмет")
    public void shouldReturnSubject() throws NotFoundException {
        Subject subject = givenSubject();
        given(subjectRepository.findByStudentAndId(any(), anyLong())).willReturn(Optional.of(subject));

        assertThat(subjectService.get(subject.getStudent(), subject.getId())).isNotNull();
    }

    @Test
    @DisplayName("Должен кинуть исключение при попытке получения предмета у другого студента")
    public void shouldNotReturnSubjectForAnotherStudent() {
        Subject subject = givenSubject();
        Student anotherStudent = new Student();
        given(subjectRepository.findByStudentAndId(eq(anotherStudent), anyLong())).willReturn(Optional.empty());

        assertThatThrownBy(() -> subjectService.get(anotherStudent, subject.getId())).isInstanceOf(NotFoundException.class);
    }

    @Test
    @DisplayName("Должен кинуть исключение при попытке получения несуществующего предмета")
    public void shouldNotReturnNotFoundSubject() {
        given(subjectRepository.findByStudentAndId(any(), anyLong())).willReturn(Optional.empty());

        assertThatThrownBy(() -> subjectService.get(new Student(), 1)).isInstanceOf(NotFoundException.class);
    }

    // create

    @Test
    @DisplayName("Должен создать предмет")
    public void shouldCreateSubject() throws AlreadyExistsException {
        given(subjectRepository.existsByStudentAndTitle(any(), any())).willReturn(false);

        assertThat(subjectService.create(new Student(), new SubjectDto())).isNotNull();
    }

    @Test
    @DisplayName("Должен кинуть исключение при попытке создания предмета с дублирующимся названием")
    public void shouldNotCreateDuplicatedSubject() {
        given(subjectRepository.existsByStudentAndTitle(any(), any())).willReturn(true);

        assertThatThrownBy(() -> subjectService.create(new Student(), new SubjectDto())).isInstanceOf(AlreadyExistsException.class);
    }

    // update

    @Test
    @DisplayName("Должен обновить название предмета")
    public void shouldUpdateSubject() throws NotFoundException, AlreadyExistsException {
        Subject subject = givenSubject();
        given(subjectRepository.findByStudentAndId(any(), anyLong())).willReturn(Optional.of(subject));
        given(subjectRepository.existsByIdNotAndStudentAndTitle(anyLong(), any(), any())).willReturn(false);

        assertThat(subjectService.update(subject.getStudent(), subject.getId(), new SubjectDto())).isNotNull();
    }

    @Test
    @DisplayName("Должен кинуть исключение при попытке обновления предмета у другого студента")
    public void shouldNotUpdateSubjectForAnotherStudent() {
        Subject subject = givenSubject();
        Student anotherStudent = new Student();
        given(subjectRepository.findByStudentAndId(eq(anotherStudent), anyLong())).willReturn(Optional.empty());
        given(subjectRepository.existsByIdNotAndStudentAndTitle(anyLong(), any(), any())).willReturn(false);

        assertThatThrownBy(() -> subjectService.update(anotherStudent, subject.getId(), new SubjectDto())).isInstanceOf(NotFoundException.class);
    }

    @Test
    @DisplayName("Должен кинуть исключение при попытке обновления несуществующего предмета")
    public void shouldNotUpdateNotFoundSubject() {
        given(subjectRepository.findByStudentAndId(any(), anyLong())).willReturn(Optional.empty());

        assertThatThrownBy(() -> subjectService.update(new Student(), 1, new SubjectDto())).isInstanceOf(NotFoundException.class);
    }

    @Test
    @DisplayName("Должен кинуть исключение при попытке задания предмету с дублирующегося названия")
    public void shouldNotUpdateDuplicatedSubject() {
        Subject subject = givenSubject();
        given(subjectRepository.findByStudentAndId(any(), anyLong())).willReturn(Optional.of(subject));
        given(subjectRepository.existsByIdNotAndStudentAndTitle(anyLong(), any(), any())).willReturn(true);

        assertThatThrownBy(() -> subjectService.update(subject.getStudent(), subject.getId(), new SubjectDto())).isInstanceOf(AlreadyExistsException.class);
    }

    // delete

    @Test
    @DisplayName("Должен удалить предмет у студента")
    public void shouldDeleteSubject() {
        Subject subject = givenSubject();
        given(subjectRepository.findByStudentAndId(any(), anyLong())).willReturn(Optional.of(subject));

        assertThatCode(() -> subjectService.delete(subject.getStudent(), subject.getId())).doesNotThrowAnyException();
    }

    @Test
    @DisplayName("Должен кинуть исключение при попытке удаления предмета у другого студента")
    public void shouldNotDeleteSubjectForAnotherStudent() {
        Subject subject = givenSubject();
        Student anotherStudent = new Student();
        given(subjectRepository.findByStudentAndId(eq(anotherStudent), anyLong())).willReturn(Optional.empty());

        assertThatThrownBy(() -> subjectService.delete(anotherStudent, subject.getId())).isInstanceOf(NotFoundException.class);
    }

    @Test
    @DisplayName("Должен кинуть исключение при попытке удаления несуществующего предмета")
    public void shouldNotDeleteNotFoundSubject() {
        given(subjectRepository.findByStudentAndId(any(), anyLong())).willReturn(Optional.empty());

        assertThatThrownBy(() -> subjectService.delete(new Student(), 1)).isInstanceOf(NotFoundException.class);
    }
}

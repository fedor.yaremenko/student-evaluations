package fred.demo.studentevaluations.services;

import fred.demo.studentevaluations.mapping.MappingHelper;
import fred.demo.studentevaluations.model.Student;
import fred.demo.studentevaluations.repositories.SubjectRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@DisplayName("Юнит-тесты сервиса управления оценками предметов")
@Tag("unit")
public class EvaluationServiceTest {

    private SubjectRepository subjectRepository;
    private EvaluationService evaluationService;

    @BeforeEach
    void setUp() {
        subjectRepository = Mockito.mock(SubjectRepository.class);
        MappingHelper mappingHelper = new MappingHelper();
        evaluationService = new EvaluationService(mappingHelper, subjectRepository);
    }

    @Test
    @DisplayName("Должен возвращать среднюю оценку по студенту")
    public void shouldReturnAverageEvaluation() {
        given(subjectRepository.getAvgEvaluationByStudent(any())).willReturn(new BigDecimal("7.5"));
        assertThat(evaluationService.getAverageEvaluation(new Student()).getAverageEvaluation()).isEqualTo("7.5");
    }

}

package fred.demo.studentevaluations.services;

import fred.demo.studentevaluations.dtos.StudentDto;
import fred.demo.studentevaluations.exceptions.AlreadyExistsException;
import fred.demo.studentevaluations.exceptions.NotFoundException;
import fred.demo.studentevaluations.mapping.MappingHelper;
import fred.demo.studentevaluations.model.Student;
import fred.demo.studentevaluations.repositories.StudentRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;

@DisplayName("Юнит-тесты сервиса управления студентами")
@Tag("unit")
public class StudentServiceTest {

    private StudentRepository studentRepository;
    private StudentService studentService;

    @BeforeEach
    void setUp() {
        studentRepository = Mockito.mock(StudentRepository.class);
        MappingHelper mappingHelper = new MappingHelper();
        studentService = new StudentService(mappingHelper, studentRepository);
    }

    // list

    @Test
    @DisplayName("Должен вернуть список студентов")
    public void shouldReturnStudentList() {
        given(studentRepository.findAll()).willReturn(List.of(new Student(), new Student()));

        assertThat(studentService.findAll()).hasSize(2);
    }

    // read

    @Test
    @DisplayName("Должен найти студента")
    public void shouldReturnStudent() throws Exception {
        given(studentRepository.findById(any())).willReturn(Optional.of(new Student()));

        assertThat(studentService.get(1)).isNotNull();
    }

    @Test
    @DisplayName("Должен кинуть исключение при попытке получения несуществующего студента")
    public void shouldNotReturnNotFoundStudent() {
        given(studentRepository.findById(any())).willReturn(Optional.empty());

        assertThatThrownBy(() -> studentService.get(1)).isInstanceOf(NotFoundException.class);
    }

    // create

    @Test
    @DisplayName("Должен создать студента")
    public void shouldCreateStudent() throws Exception {
        given(studentRepository.existsByLastNameAndFirstNameAndSurName(any(), any(), any())).willReturn(false);

        assertThat(studentService.create(new StudentDto())).isNotNull();
    }

    @Test
    @DisplayName("Должен кинуть исключение при попытке создания студента с дублирующимся именем")
    public void shouldNotCreateDuplicatedStudent() {
        given(studentRepository.existsByLastNameAndFirstNameAndSurName(any(), any(), any())).willReturn(true);

        assertThatThrownBy(() -> studentService.create(new StudentDto())).isInstanceOf(AlreadyExistsException.class);
    }

    // update

    @Test
    @DisplayName("Должен обновить имя студента")
    public void shouldUpdateStudent() throws NotFoundException, AlreadyExistsException {
        given(studentRepository.existsByIdNotAndLastNameAndFirstNameAndSurName(anyLong(), any(), any(), any())).willReturn(false);
        given(studentRepository.findById(any())).willReturn(Optional.of(new Student()));

        assertThat(studentService.update(1, new StudentDto())).isNotNull();
    }

    @Test
    @DisplayName("Должен кинуть исключение при попытке обновления несуществующего студента")
    public void shouldNotUpdateNotFoundStudent() {
        given(studentRepository.existsByIdNotAndLastNameAndFirstNameAndSurName(anyLong(), any(), any(), any())).willReturn(false);
        given(studentRepository.findById(any())).willReturn(Optional.empty());

        assertThatThrownBy(() -> studentService.update(1, new StudentDto())).isInstanceOf(NotFoundException.class);
    }

    @Test
    @DisplayName("Должен кинуть исключение при попытке задания студенту дублирующегося имени")
    public void shouldNotUpdateDuplicatedStudent() {
        given(studentRepository.existsByIdNotAndLastNameAndFirstNameAndSurName(anyLong(), any(), any(), any())).willReturn(true);

        assertThatThrownBy(() -> studentService.update(1, new StudentDto())).isInstanceOf(AlreadyExistsException.class);
    }

    // delete

    @Test
    @DisplayName("Должен удалить студента")
    public void shouldDeleteStudent() {
        given(studentRepository.findById(any())).willReturn(Optional.of(new Student()));

        assertThatCode(() -> studentService.delete(1)).doesNotThrowAnyException();
    }

    @Test
    @DisplayName("Должен кинуть исключение при попытке удаления несуществующиего студента")
    public void shouldNotDeleteNotFoundStudent() {
        given(studentRepository.findById(any())).willReturn(Optional.empty());

        assertThatThrownBy(() -> studentService.delete(1)).isInstanceOf(NotFoundException.class);
    }
}

package fred.demo.studentevaluations.dtos;

import org.junit.jupiter.api.Tag;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

@Tag("unit")
public abstract class BasicDtoValidationTest {

    private final Validator validator =  Validation.buildDefaultValidatorFactory().getValidator();

    protected void assertValidDto(Object dto) {
        Set<ConstraintViolation<Object>> violations = validator.validate(dto);
        assertThat(violations).isEmpty();
    }

    protected void assertWrongPath(Object dto, String wrongPath) {
        Set<ConstraintViolation<Object>> violations = validator.validate(dto);
        assertThat(violations).hasSize(1).element(0)
                .matches(violation -> violation.getPropertyPath().toString().equals(wrongPath));
    }

}

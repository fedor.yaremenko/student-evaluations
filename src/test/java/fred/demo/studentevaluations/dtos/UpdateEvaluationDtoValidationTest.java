package fred.demo.studentevaluations.dtos;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class UpdateEvaluationDtoValidationTest extends BasicDtoValidationTest {

    private UpdateEvaluationDto givenValidSubjectDto() {
        UpdateEvaluationDto updateEvaluationDto = new UpdateEvaluationDto();
        updateEvaluationDto.setEvaluation(5);
        return updateEvaluationDto;
    }

    @Test
    @DisplayName("Должен разрешать валидный DTO")
    public void shouldAcceptValidDto() {
        UpdateEvaluationDto updateEvaluationDto = givenValidSubjectDto();

        assertValidDto(updateEvaluationDto);
    }

    @Test
    @DisplayName("Должен разрешить очистить оценку")
    public void shouldAcceptNullEvaluation() {
        UpdateEvaluationDto updateEvaluationDto = givenValidSubjectDto();
        updateEvaluationDto.setEvaluation(null);

        assertValidDto(updateEvaluationDto);
    }

    @Test
    @DisplayName("Должен давать ошибку на слишком низкую оценку")
    public void shouldNotAcceptTooLowEvaluation() {
        UpdateEvaluationDto updateEvaluationDto = givenValidSubjectDto();
        updateEvaluationDto.setEvaluation(0);

        assertWrongPath(updateEvaluationDto, "evaluation");
    }

    @Test
    @DisplayName("Должен давать ошибку на слишком высокую оценку")
    public void shouldNotAcceptTooHighEvaluation() {
        UpdateEvaluationDto updateEvaluationDto = givenValidSubjectDto();
        updateEvaluationDto.setEvaluation(11);

        assertWrongPath(updateEvaluationDto, "evaluation");
    }

}

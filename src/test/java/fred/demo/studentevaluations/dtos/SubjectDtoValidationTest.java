package fred.demo.studentevaluations.dtos;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static fred.demo.studentevaluations.Constants.MAX_STRING_LENGTH;

public class SubjectDtoValidationTest extends BasicDtoValidationTest {

    private SubjectDto givenValidSubjectDto() {
        SubjectDto subjectDto = new SubjectDto();
        subjectDto.setTitle("Функциональный анализ");
        return subjectDto;
    }

    @Test
    @DisplayName("Должен разрешать валидный DTO")
    public void shouldAcceptValidDto() {
        SubjectDto subjectDto = givenValidSubjectDto();

        assertValidDto(subjectDto);
    }

    @Test
    @DisplayName("Должен давать ошибку на пустое название")
    public void shouldNotAcceptBlankTitle() {
        SubjectDto subjectDto = givenValidSubjectDto();
        subjectDto.setTitle("");

        assertWrongPath(subjectDto, "title");
    }

    @Test
    @DisplayName("Должен давать ошибку на слишком длинное название")
    public void shouldNotAcceptTooLongLastName() {
        SubjectDto subjectDto = givenValidSubjectDto();
        subjectDto.setTitle("A".repeat(MAX_STRING_LENGTH + 1));

        assertWrongPath(subjectDto, "title");
    }

}

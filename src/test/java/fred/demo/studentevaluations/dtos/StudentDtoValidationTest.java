package fred.demo.studentevaluations.dtos;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static fred.demo.studentevaluations.Constants.MAX_STRING_LENGTH;

@DisplayName("Юнит-тесты валидации DTO студента")
public class StudentDtoValidationTest extends BasicDtoValidationTest {

    private StudentDto givenValidStudentDto() {
        StudentDto studentDto = new StudentDto();
        studentDto.setLastName("Иванов");
        studentDto.setFirstName("Иван");
        studentDto.setSurName("Иванович");
        return studentDto;
    }

    @Test
    @DisplayName("Должен разрешать валидный DTO")
    public void shouldAcceptValidDto() {
        StudentDto studentDto = givenValidStudentDto();

        assertValidDto(studentDto);
    }

    @Test
    @DisplayName("Должен давать ошибку на пустую фамилию")
    public void shouldNotAcceptBlankLastName() {
        StudentDto studentDto = givenValidStudentDto();
        studentDto.setLastName("");

        assertWrongPath(studentDto, "lastName");
    }

    @Test
    @DisplayName("Должен давать ошибку на слишком длинную фамилию")
    public void shouldNotAcceptTooLongLastName() {
        StudentDto studentDto = givenValidStudentDto();
        studentDto.setLastName("A".repeat(MAX_STRING_LENGTH + 1));

        assertWrongPath(studentDto, "lastName");
    }

    @Test
    @DisplayName("Должен давать ошибку на пустое имя")
    public void shouldNotAcceptBlankFirstName() {
        StudentDto studentDto = givenValidStudentDto();
        studentDto.setFirstName("");

        assertWrongPath(studentDto, "firstName");
    }

    @Test
    @DisplayName("Должен давать ошибку на слишком длинное имя")
    public void shouldNotAcceptTooLongFirstName() {
        StudentDto studentDto = givenValidStudentDto();
        studentDto.setFirstName("A".repeat(MAX_STRING_LENGTH + 1));

        assertWrongPath(studentDto, "firstName");
    }

    @Test
    @DisplayName("Должен давать ошибку на пустое отчество")
    public void shouldNotAcceptBlankSurName() {
        StudentDto studentDto = givenValidStudentDto();
        studentDto.setSurName("");

        assertWrongPath(studentDto, "surName");
    }

    @Test
    @DisplayName("Должен давать ошибку на слишком длинное отчество")
    public void shouldNotAcceptTooLongSurName() {
        StudentDto studentDto = givenValidStudentDto();
        studentDto.setSurName("A".repeat(MAX_STRING_LENGTH + 1));

        assertWrongPath(studentDto, "surName");
    }
}
